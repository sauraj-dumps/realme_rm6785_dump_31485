## aosp_RM6785-userdebug 12 SQ1A.211205.008 eng.androi.20211219.180753 test-keys
- Manufacturer: realme
- Platform: mt6785
- Codename: RM6785
- Brand: realme
- Flavor: aosp_RM6785-userdebug
- Release Version: 12
- Id: SQ1A.211205.008
- Incremental: eng.androi.20211219.180753
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/RMX2001/RMX2001L1:10/QP1A.190711.020/1594211000:user/release-keys
- OTA version: 
- Branch: aosp_RM6785-userdebug-12-SQ1A.211205.008-eng.androi.20211219.180753-test-keys
- Repo: realme_rm6785_dump_31485


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
